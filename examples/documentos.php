<?php

require '../vendor/autoload.php';

$cliente = new Archiving\SDK\Client([
  'base_url' => 'http://localhost:8090/',
  'email' => 'desarrollo@stx.com.mx',
  'apikey' => 'mFYExYtlY4s0jhHT8mXW8',
  'scope' => 'barebone'
]);

dump( Archiving\SDK\Documento::consultar('saldos', $cliente) );

$documento = new Archiving\SDK\Documento('saldos', $cliente);

dump('crear', $documento->crear('a'));

if ($documento->hasErrors()){
  dump( $documento->getErrors() );
  exit('error');
}

$documento = Archiving\SDK\Documento::cargar('saldos', $documento->folio, $documento->serie, $cliente);

dump('cargar',  $documento );

dump('actualizar', $documento->actualizar(['some' => range(0,5)])->toArray());

dump('limpiar', $documento->limpiar(['some'])->toArray());

dump('cancelar', $documento->cancelar()->toArray());

dump('descancelar', $documento->descancelar()->toArray());

$comentario = $documento->comentar('...');

dump('comentar', $comentario);

dump($documento->toArray());

dump('descomentar', $documento->descomentar($comentario->id));

dump($documento->toArray());

$comentario = $documento->comentar('...');
$comentario = $documento->comentar('...');
$comentario = $documento->comentar('...');
$comentario = $documento->comentar('...');

dump('comentarios', $documento->comentarios() );

dump('eventos', $documento->eventos() );

dump($documento);



