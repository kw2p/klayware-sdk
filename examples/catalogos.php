<?php

require '../vendor/autoload.php';

$cliente = new Archiving\SDK\Client([
  'base_url' => 'http://localhost:8090/',
  'email' => 'desarrollo@stx.com.mx',
  'apikey' => 'mFYExYtlY4s0jhHT8mXW8',
  'scope' => 'barebone'
]);

dump( Archiving\SDK\Catalogo::consultar('estados', $cliente) );

$catalogo = new Archiving\SDK\Catalogo('estados', $cliente);

dump('actualizar', $catalogo->actualizar([
  "estado" => "veracruz",
  "alias" => "ver",
  "country_id" => 5
])->toArray());

if ($catalogo->hasErrors()){
  dump( $catalogo->getErrors() );
  exit('error');
}

$catalogo = Archiving\SDK\Catalogo::cargar('estados', $catalogo->id, $cliente);

dump('cargar',  $catalogo);

dump('actualizar', $catalogo->actualizar([
  "estado" => "veracruz",
  "alias" => "ver",
  "country_id" => 9
])->toArray());

dump('eliminar', $catalogo->eliminar());
