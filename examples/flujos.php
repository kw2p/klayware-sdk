<?php

require '../vendor/autoload.php';

$cliente = new Archiving\SDK\Client([
  'base_url' => 'http://localhost:8090/',
  'email' => 'desarrollo@stx.com.mx',
  'apikey' => 'mFYExYtlY4s0jhHT8mXW8',
  'scope' => 'barebone'
]);

dump( Archiving\SDK\Flujo::consultar('compras', $cliente) );

$flujo = new Archiving\SDK\Flujo('compras', $cliente);

dump('crear', $flujo->crear('uno', 'a'));

if ($flujo->hasErrors()){
  dump( $flujo->getErrors() );
  exit('error');
}

$flujo = Archiving\SDK\Flujo::cargar('compras', $flujo->folio, $flujo->serie, $cliente);

dump('cargar',  $flujo );

dump('actualizar', $flujo->actualizar(['some' => range(0,5)])->toArray());

if ($flujo->hasErrors()){
  dump( $flujo->getErrors() );
  exit('error');
}

dump('limpiar', $flujo->limpiar(['some'])->toArray());

dump('avanzar', $flujo->avanzar('dos')->toArray());

dump('retroceder', $flujo->retroceder()->toArray());

dump('cerrar', $flujo->cerrar()->toArray());

dump('abrir', $flujo->abrir()->toArray());

dump('cancelar', $flujo->cancelar()->toArray());

dump('descancelar', $flujo->descancelar()->toArray());

$comentario = $flujo->comentar('...');

dump('comentar', $comentario);

dump($flujo->toArray());

dump('descomentar', $flujo->descomentar($comentario->id));

dump($flujo->toArray());

$comentario = $flujo->comentar('...');
$comentario = $flujo->comentar('...');
$comentario = $flujo->comentar('...');
$comentario = $flujo->comentar('...');

dump('comentarios', $flujo->comentarios() );

dump('eventos', $flujo->eventos() );

dump($flujo);

