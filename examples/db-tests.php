<?php

require '../vendor/autoload.php';

use Archiving\SDK\Models\CatalogWithStorages;
use Archiving\SDK\Models\DocumentWithStorages;
use Archiving\SDK\Models\FlowWithStorages;
use Archiving\SDK\Models\Scheme;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Support\Facades\Facade;

class DB extends Illuminate\Database\Capsule\Manager {};

# Configuración DB
$capsule = new Capsule;

$capsule->addConnection([
    'driver' =>'mysql',
    'host' => '127.0.0.1',
    'port' => '3306',
    'database' => 'kw2p_archiving_kdm',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
    'collation' => 'utf8_spanish_ci',
    'prefix' => '',
    'strict' => false
]);

$capsule->setAsGlobal();

# Iniciamos Eloquent
$capsule->bootEloquent();


$cliente = new Archiving\SDK\Client([
  'base_url' => 'http://localhost:8090/',
  'email' => 'desarrollo@stx.com.mx',
  'apikey' => 'KBhaTFJrpytPQSEgugUXT',
  'scope' => 'kdm'
]);

try {

    // $scheme = Scheme::where(['name' => 'maquinas', 'type' => 'cat'])->firstOrFail();
    // $catalog = CatalogWithStorages::setScheme($scheme)->withRootRelations(['*'])->withStorageRelations(['*']);

    // $catalog->getConnection()->enableQueryLog();

    // $query = $catalog->withRelations();
    // // $query->whereJoin('planta.payload->nombre', '=', 'Centro');
    // // $query->orderByJoin('planta.payload->prefijo', 'desc');

    # ---

    // $scheme = Scheme::where(['name' => 'asignacion', 'type' => 'doc'])->firstOrFail();
    // $document = DocumentWithStorages::setScheme($scheme)->withRootRelations(['*'])->withStorageRelations(['*']);

    // $document->getConnection()->enableQueryLog();

    // $query = $document->withRelations();
    // $query->whereJoin('planta.payload->prefijo', '=', 'cto');

    # ---

    $scheme = Scheme::where(['name' => 'orden_produccion', 'type' => 'flu'])->firstOrFail();
    $flow = FlowWithStorages::setScheme($scheme)->withRootRelations(['*'])->withStorageRelations(['*']);

    $flow->getConnection()->enableQueryLog();

    $query = $flow->withRelations();
    $query->whereJoin('planta.payload->prefijo', '=', 'cto');

    dump( $query->get()->toArray() );

    dd(DB::getQueryLog());

} catch (\Throwable $e) {

    dd($e);
}


// dd($catalog->getConnection()->getQueryLog());
// dd( $catalog );


// dump( Archiving\SDK\Catalogo::consultar('maquinas', $cliente) );
