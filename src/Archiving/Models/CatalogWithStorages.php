<?php

namespace Archiving\SDK\Models;

use Archiving\SDK\Models\Catalog;
use Archiving\SDK\Traits\EloquentJoin;
use Archiving\SDK\Traits\HasDynamicRelations;

class CatalogWithStorages extends Catalog
{
    use HasDynamicRelations, EloquentJoin;

    protected $useTableAlias = true;

    public function getStorage()
    {
        return [null => $this->scheme->schema[$this->storageName]];
    }
}
