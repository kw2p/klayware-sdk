<?php

namespace Archiving\SDK\Models;

use Archiving\SDK\Models\Document;
use Archiving\SDK\Traits\EloquentJoin;
use Archiving\SDK\Traits\HasDynamicRelations;

class DocumentWithStorages extends Document
{
    use HasDynamicRelations, EloquentJoin;

    protected $useTableAlias = true;
}
