<?php

namespace Archiving\SDK\Models;

use Archiving\SDK\Exceptions\ValidationSchemeException;
use Archiving\SDK\Models\KlayModel;
use Archiving\SDK\Traits\HasCompositePrimaryKey;
use Archiving\SDK\Traits\HasScheme;
use Archiving\SDK\Traits\HasStorageBuilder;

class Flow extends KlayModel
{
  use HasCompositePrimaryKey,
      HasScheme,
      HasStorageBuilder;

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'folio';

  /**
   * The composite keys for the model.
   *
   * @var array
   */
  protected $compositeKeys = [
    'folio',
    'serie'
  ];

  /**
   * The model's attributes.
   *
   * @var array
   */
  protected $attributes = [
    'payload' => '[]'
  ];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'folio',
    'serie',
    'etapa',
    'estado',
    'transiciones',
    'payload',
    'author'
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'transiciones' => 'array',
    'payload' => 'array',
    'author' => 'array',
  ];

  /**
   * The accessors to append to the model's array form.
   *
   * @var array
   */
  protected $appends = [
    'schema'
  ];

  /**
   * The attributes that should be hidden for serialization.
   *
   * @var array
   */
  protected $hidden = [
    'scheme',
    'transiciones',
  ];

  public function getKeyNameForRelations()
  {
    return $this->getCompositeKeys();
  }

  public function buildCallback($table)
  {
    $table->unsignedInteger('folio');
    $table->string('serie', 10);
    $table->string('etapa', 100);
    $table->string('estado', 10);
    $table->json('transiciones');
    $table->json('payload');
    $table->json('author');
    $table->timestamps();
    $table->engine = 'MyISAM';
  }

  public function buildAfter()
  {
    \DB::statement("ALTER TABLE `flu_{$this->scheme->name}` DROP `id`, CHANGE `folio` `folio` int(10) unsigned NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY `serie_folio` (`serie`, `folio`)");
  }

  public function create($etapa, $serie, $payload, $author = [])
  {
    $this->validateData($payload, true);

    if (!optional($this->scheme->etapas)[$etapa]) {
      throw new \Klayware\Exceptions\KlayException("Etapa [etapa: {$etapa}] no existe en definición del esquema", 'etapa_nonexist');
    }
    if (!$this->scheme->etapas[$etapa]['inicial']) {
      throw new \Klayware\Exceptions\KlayException("Etapa [etapa: {$etapa}] no esta definida como inicial", 'etapa_noninitial');
    }

    $this->fill([
      'serie' => $serie,
      'etapa' => $etapa,
      'estado' => $this->scheme->etapas[$etapa]['final'] ? 'terminado': 'abierto',
      'transiciones' => [$etapa],
      'author' => $this->getAuthor($author)
    ])->save();

    $this->updatePayload($payload, function($old_value, &$new_value, $trace_path) {
      $this->findUploadedFiles($old_value, $new_value, $trace_path);
    })->save();

    $this->addEvent('create', array_except(get_defined_vars(), ['author']), $this->getAuthor($author));
    return $this;
  }

  public function update(array $payload = [], $author = [])
  {
    $this->validateData($this->updateArray($this->payload, $payload), true);

    $this->updatePayload($payload, function($old_value, &$new_value, $trace_path) {
      $this->findUploadedFiles($old_value, $new_value, $trace_path);
    });

    if ($this->isDirty()) {
      $this->addEvent('update', $this->getDiffChanges(), $this->getAuthor($author));
    }
    return $this->save();
  }

  public function clear($payload, $author = [])
  {
    try {
      $this->validateData($this->clearArray($this->payload, $payload), true);

      $cleared = [];

      $this->clearPayload($payload, function($old_value, &$new_value, $trace_path) use (&$cleared) {
        $cleared[] = [$old_value, $new_value, $trace_path, true];
      });

      foreach ($cleared as $clear) {
        $this->findUploadedFiles(...$clear);
      }

    } catch (ValidationSchemeException $e) {
      throw  new \Klayware\Exceptions\KlayException("Imposible limpiar. {$e->getMessage()}", 'payload_invalid');
    } catch (\Exception $e) {
      throw  new \Klayware\Exceptions\KlayException($e->getMessage(), 'payload_invalid');
    }

    if ($this->isDirty()) {
      $this->addEvent('clear', $this->getDiffChanges(), $this->getAuthor($author));
    }
    return $this->save();
  }

  public function replace($payload, $author = [])
  {
    try {
      $this->validateData($this->replaceArray($this->payload, $payload), true);

      $this->replacePayload($payload);

    } catch (ValidationSchemeException $e) {
      throw  new \Klayware\Exceptions\KlayException("Imposible remplazar. {$e->getMessage()}", 'payload_invalid');
    } catch (\Exception $e) {
      throw  new \Klayware\Exceptions\KlayException($e->getMessage(), 'payload_invalid');
    }

    if ($this->isDirty()) {
      $this->addEvent('replace', $this->getDiffChanges(), $this->getAuthor($author));
    }
    return $this->save();
  }

  public function advance($etapa, $payload, $author = [])
  {
    $this->validateData($this->updateArray($this->payload, $payload), true);

    if ($this->etapa == $etapa) {
      $etapa = $this->scheme->schema['etapas'][$this->etapa]['title'] ?? $this->etapa;
      throw new \Klayware\Exceptions\KlayException("Flujo ya se encuentra en {$etapa}", 'transition_equals');
    }

    $transicion_valida = false;
    foreach ($this->scheme->transiciones as $transicion) {
      if ($transicion['origen'] == $this->etapa && $transicion['destino'] == $etapa) {
        $transicion_valida = true;
        break;
      }
    }
    if (!$transicion_valida) {
      throw new \Klayware\Exceptions\KlayException("Transición [origen: {$this->etapa}, destino: {$etapa}] invalida", 'transition_invalid');
    }
    $this->etapa = $etapa;
    $this->estado = $this->scheme->etapas[$etapa]['final'] ? 'terminado': 'abierto';
    $this->transiciones = array_merge($this->transiciones, [$etapa]);

    $this->updatePayload($payload, function($old_value, &$new_value, $trace_path) {
      $this->findUploadedFiles($old_value, $new_value, $trace_path);
    });

    $this->addEvent('advance', $this->getDiffChanges(), $this->getAuthor($author));
    return $this->save();
  }

  public function retreat($author = []) {
    if (count($this->transiciones) == 1) {
      throw  new \Klayware\Exceptions\KlayException('Flujo se encuentra en transición inicial', 'transition_invalid');
    }
    $this->transiciones = array_slice($this->transiciones, 0, -1);
    $this->etapa = last($this->transiciones);
    $this->estado = 'abierto';
    $this->addEvent('retreat', $this->getDiffChanges(), $this->getAuthor($author));
    return $this->save();
  }

  public function open($author = []) {
    if ($this->estado != 'cerrado') {
      throw  new \Klayware\Exceptions\KlayException("Flujo [estado: {$this->estado}] debe encontrarse cerrado", 'cannot_open');
    }
    $this->estado = 'abierto';
    $this->addEvent('open', $this->getDiffChanges(), $this->getAuthor($author));
    return $this->save();
  }

  public function close($author = []) {
    if ($this->estado != 'abierto') {
      throw  new \Klayware\Exceptions\KlayException("Flujo [estado: {$this->estado}] debe encontrarse abierto", 'cannot_close');
    }
    $this->estado = 'cerrado';
    $this->addEvent('close', $this->getDiffChanges(), $this->getAuthor($author));
    return $this->save();
  }

  public function cancel($author = []) {
    if ($this->estado == 'cancelado') {
      throw  new \Klayware\Exceptions\KlayException('Flujo ya se encuentra en cancelado', 'cannot_cancel');
    }
    $this->estado = 'cancelado';
    $this->addEvent('cancel', $this->getDiffChanges(), $this->getAuthor($author));
    return $this->save();
  }

  public function uncancel($author = []) {
    if ($this->estado != 'cancelado') {
      throw  new \Klayware\Exceptions\KlayException("Flujo [estado: {$this->estado}] debe encontrarse cancelado", 'cannot_uncancel');
    }
    $event = $this->event()->where('event', 'cancel')->orderby('id', 'desc')->first();
    $this->estado = $event->details['original']['estado'];
    $this->addEvent('uncancel', $this->getDiffChanges(), $this->getAuthor($author));
    return $this->save();
  }

  public function comment($comment, $author = []) {
    $event = $this->addEvent('comment', [
      'comment' => $comment,
    ], $this->getAuthor($author));
    return $this->addComment($comment, $this->getAuthor($author));
  }

  public function uncomment($comment_id, $author = []) {
    $comment = $this->comments()->findOrFail($comment_id);
    $this->addEvent('uncomment', [
      'comment_id' => $comment_id
    ], $this->getAuthor($author));
    return $comment->delete();
  }

  private function getAuthor($author = [])
  {
    return array_merge($author, [
      'scope' => $author['scope'] ?? kw2p_ambito(),
      'id' => $author['id'] ?? auth()->user()->getKey(),
      'correo' => $author['correo'] ?? auth()->user()->correo
    ]);
  }

  public function validate(array $payload = [], $folio = null, $serie = null)
  {
    tap($this->firstOrNew(['folio' => $folio, 'serie' => $serie ?? '']), function ($instance) use ($payload) {
      $instance->validateData($instance->updateArray($instance->payload, $payload), true);
    });
    return true;
  }

  public function validateClear(array $payload = [], $folio = null, $serie = null)
  {
    try {
      tap($this->firstOrNew(['folio' => $folio, 'serie' => $serie ?? '']), function ($instance) use ($payload) {
        $instance->validateData($instance->clearArray($instance->payload, $payload), true);
      });
    } catch (ValidationSchemeException $e) {
      throw  new \Klayware\Exceptions\KlayException("Imposible limpiar. {$e->getMessage()}", 'payload_invalid');
    } catch (\Exception $e) {
      throw  new \Klayware\Exceptions\KlayException($e->getMessage(), 'payload_invalid');
    }
    return true;
  }

  public function validateReplace(array $payload = [], $folio = null, $serie = null)
  {
    try {
      tap($this->firstOrNew(['folio' => $folio, 'serie' => $serie ?? '']), function ($instance) use ($payload) {
        $instance->validateData($instance->replaceArray($instance->payload, $payload), true);
      });
    } catch (ValidationSchemeException $e) {
      throw  new \Klayware\Exceptions\KlayException("Imposible remplazar. {$e->getMessage()}", 'payload_invalid');
    } catch (\Exception $e) {
      throw  new \Klayware\Exceptions\KlayException($e->getMessage(), 'payload_invalid');
    }
    return true;
  }

  /**
   * Convert the object into something JSON serializable.
   *
   * @return array
   */
  public function jsonSerialize()
  {
      $serialize = $this->toArray();
      $serialize['payload'] = (object) $serialize['payload'];
      return $serialize;
  }
}
