<?php

namespace Archiving\SDK\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class KlayModel extends EloquentModel
{
    /**
     * Get the model's relationships in array form.
     *
     * @return array
     */
    public function relationsToArray()
    {
        $attributes = parent::relationsToArray();
        return !empty($attributes) ? ['_relaciones' => $attributes] : [];
    }

    public function newModelQuery()
    {
        $this->setConnection(kw2p_ambito() . 'Archiving');
        return parent::newModelQuery();
    }
}
