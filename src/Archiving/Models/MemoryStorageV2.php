<?php

namespace Archiving\SDK\Models;

class MemoryStorageV2
{
    protected static $memoryStorage = [];

    final public function memoize($key, callable $callback)
    {
        if (is_array($key)) {
            $key = implode('-', $key);
        }
        if (!array_key_exists($key, static::$memoryStorage)) {
          static::$memoryStorage[$key] = $callback();
        }
        return static::$memoryStorage[$key];
    }
  }
