<?php

namespace Archiving\SDK\Models\Relations;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

trait HasOneOrMany
{
    protected $path = null;

    protected $isRootRelation = true;

    protected $isCompositeKey = false;

    protected $hasManyType = false;

    protected $hasOneType = false;

    public function extra($extra)
    {
        $this->path = $extra['path'] ?? null;
        $this->isRootRelation = $this->path === null;
        $this->isCompositeKey = $extra['isCompositeKey'] ?? false;
        $this->hasManyType = $extra['hasManyType'] ?? false;
        $this->hasOneType = $extra['hasOneType'] ?? false;

        return $this;
    }

    /**
     * Set the base constraints on the relation query.
     *
     * @return void
     */
    public function addConstraints()
    {
        if (static::$constraints) {
            $foreignKey = $this->getForeignKeyName();
            $parentKeyValue = $this->getParentKey();
            //If the foreign key is an array (multi-column relationship), we adjust the query.
            if (is_array($this->foreignKey)) {
                $parentKeyValue = array_values(array_wrap($parentKeyValue));
                foreach ($this->foreignKey as $index => $key) {
                    // list(, $key) = explode('.', $key);
                    $fullKey = $this->getRelated()->getTable().'.'.$key;
                    $this->query->where($fullKey, '=', $parentKeyValue[$index] ?? null);
                    $this->query->whereNotNull($fullKey);
                }
            } else {
                $fullKey = $this->getRelated()->getTable().'.'.$foreignKey;
                if (is_array($parentKeyValue)) {
                    $this->query->whereIn($fullKey, $parentKeyValue);
                } else {
                    $this->query->where($fullKey, '=', $parentKeyValue);
                }
                $this->query->whereNotNull($fullKey);
            }
        }
    }

    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param  array  $models
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        if ($this->isCompositeKey) {
            foreach ($this->foreignKey as $index => $key) {
                $keys = $this->getKeys($models, $this->localKey[$index]);

                $keys = array_unique(array_flatten($keys));

                $this->query->whereIn($key, $keys);
            }
        } else {
            $keys = $this->getKeys($models, $this->localKey);

            $keys = array_unique(array_flatten($keys));

            $this->query->whereIn($this->foreignKey, $keys);
        }
    }

    /**
     * Get the fully qualified parent key name.
     *
     * @return string
     */
    public function getQualifiedParentKeyName()
    {
        if (is_array($this->localKey)) { //Check for multi-columns relationship
            return array_map(function ($k) {
                return $this->parent->getTable().'.'.$k;
            }, $this->localKey);
        } else {
            return $this->parent->getTable().'.'.$this->localKey;
        }
    }

    /**
     * Get the plain foreign key.
     *
     * @return string
     */
    public function getForeignKeyName()
    {
        $key = $this->getQualifiedForeignKeyName();

        if (is_array($key)) { //Check for multi-columns relationship
            return array_map(function ($k) {
                $segments = explode('.', $k);

                return $segments[count($segments) - 1];
            }, $key);
        } else {
            $segments = explode('.', $key);

            return $segments[count($segments) - 1];
        }
    }

    /**
     * Attach a model instance to the parent model.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function save(Model $model)
    {
        $foreignKey = $this->getForeignKeyName();
        $parentKeyValue = $this->getParentKey();

        if (is_array($foreignKey)) { //Check for multi-columns relationship
            foreach ($foreignKey as $index => $key) {
                $model->setAttribute($key, $parentKeyValue[$index]);
            }
        } else {
            $model->setAttribute($foreignKey, $parentKeyValue);
        }

        return $model->save() ? $model : false;
    }

    /**
     * Create a new instance of the related model.
     *
     * @param  array  $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $attributes = [])
    {
        return tap($this->related->newInstance($attributes), function ($instance) {
            $foreignKey = $this->getForeignKeyName();
            $parentKeyValue = $this->getParentKey();

            if (is_array($foreignKey)) { //Check for multi-columns relationship
                foreach ($foreignKey as $index => $key) {
                    $instance->setAttribute($key, $parentKeyValue[$index]);
                }
            } else {
                $instance->setAttribute($foreignKey, $parentKeyValue);
            }

            $instance->save();
        });
    }

    /**
     * Add the constraints for a relationship query on the same table.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Builder  $parentQuery
     * @param  array|mixed  $columns
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRelationExistenceQueryForSelfRelation(Builder $query, Builder $parentQuery, $columns = ['*'])
    {
        $query->from($query->getModel()
                ->getTable().' as '.$hash = $this->getRelationCountHash());

        $query->getModel()
            ->setTable($hash);

        return $query->select($columns)
            ->whereColumn($this->getQualifiedParentKeyName(), '=',
                is_array($this->getForeignKeyName()) ? //Check for multi-columns relationship
                    array_map(function ($k) use ($hash) {
                        return $hash.'.'.$k;
                    }, $this->getForeignKeyName()) : $hash.'.'.$this->getForeignKeyName());
    }

    /**
     * Match the eagerly loaded results to their many parents.
     *
     * @param  array  $models
     * @param  \Illuminate\Database\Eloquent\Collection  $results
     * @param  string  $relation
     * @param  string  $type
     * @return array
     */
    protected function matchOneOrMany(array $models, Collection $results, $relation, $type)
    {
        $dictionary = $this->buildDictionary($results);

        // Once we have the dictionary we can simply spin through the parent models to
        // link them up with their children using the keyed dictionary to make the
        // matching very convenient and easy work. Then we'll just return them.
        foreach ($models as $model) {

            $key = $model->getAttribute($this->localKey);

            /**
             * Si llave compuesta
             *  [
             *   0 => ['1', '5']
             *   1 => ['a', 'b']
             *  ]
             *
             *  [
             *   0 => '1-a',
             *   1 => '5-b'
             *  ]
             *
             */
            if (($this->hasManyType || $this->hasOneType == 'array') && $this->isCompositeKey) {

                foreach ($key as $i => $k) $key[$i] = (array) $k;

                $compositeKey = [];
                foreach (reset($key) as $i => $k) $compositeKey[] = $k . '-' . $key[1][$i];

                $key = $compositeKey;
            }

            if ($this->hasManyType || $this->hasOneType == 'array') {

                $relationValue = null;

                foreach ($key ?? [] as $index => $dictKey) {
                    if (isset($dictionary[$dictKey])) {
                        if ($this->isRootRelation) {
                            $collectionValue = $this->getRelationValue($dictionary, $dictKey, $type);
                            $relationValue = $relationValue ? $relationValue->merge($collectionValue) : $collectionValue;
                        } else {
                            $path = str_replace('*', $index, $this->path);
                            $payload = $model->payload;
                            data_set($payload, $path, $this->getRelationValue($dictionary, $dictKey, $type));
                            $model->payload = $payload;
                        }
                    }
                }

                if ($this->isRootRelation) {
                    $model->setRelation($relation, $relationValue);
                }

            } else {

                //If the foreign key is an array, we know it's a multi-column relationship
                //And we join the values to construct the dictionary key
                $dictKey = is_array($key) ? implode('-', $key) : $key;

                if (isset($dictionary[$dictKey])) {
                    if ($this->isRootRelation) {
                        $model->setRelation($relation, $this->getRelationValue($dictionary, $dictKey, $type));
                    } else {
                        $payload = $model->payload;
                        data_set($payload, $this->path, $this->getRelationValue($dictionary, $dictKey, $type));
                        $model->payload = $payload;
                    }
                }
            }
        }

        return $models;
    }

    /**
     * Get the value of a relationship by one or many type.
     *
     * @param  array  $dictionary
     * @param  string  $key
     * @param  string  $type
     * @return mixed
     */
    protected function getRelationValue(array $dictionary, $key, $type)
    {
        $value = $dictionary[$key];

        return $type === 'many' && $this->isRootRelation ? $this->related->newCollection($value) : reset($value);
    }

    /**
     * Build model dictionary keyed by the relation's foreign key.
     *
     * @param  \Illuminate\Database\Eloquent\Collection  $results
     * @return array
     */
    protected function buildDictionary(Collection $results)
    {
        $dictionary = [];

        $foreign = $this->getForeignKeyName();

        // First we will create a dictionary of models keyed by the foreign key of the
        // relationship as this will allow us to quickly access all of the related
        // models without having to do nested looping which will be quite slow.
        foreach ($results as $result) {
            //If the foreign key is an array, we know it's a multi-column relationship...
            if (is_array($foreign)) {
                $dictKeyValues = array_map(function ($k) use ($result) {
                    return data_get($result, str_replace('->', '.', $k));
                }, $foreign);
                //... so we join the values to construct the dictionary key
                $dictionary[implode('-', $dictKeyValues)][] = $result;
            } else {
                $dictionary[data_get($result, str_replace('->', '.', $foreign))][] = $result;
            }
        }

        return $dictionary;
    }

    /**
     * Set the foreign ID for creating a related model.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    protected function setForeignAttributesForCreate(Model $model)
    {
        $foreignKey = $this->getForeignKeyName();
        $parentKeyValue = $this->getParentKey();
        if (is_array($foreignKey)) { //Check for multi-columns relationship
            foreach ($foreignKey as $index => $key) {
                $model->setAttribute($key, $parentKeyValue[$index]);
            }
        } else {
            parent::setForeignAttributesForCreate($model);
        }
    }
}
