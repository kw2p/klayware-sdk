<?php

namespace Archiving\SDK\Models\Relations;

use Illuminate\Database\Eloquent\Collection;
use Fico7489\Laravel\EloquentJoin\Relations\HasManyJoin as BaseHasMany;

class HasMany extends BaseHasMany
{
    use HasOneOrMany;

    /**
     * Initialize the relation on a set of models.
     *
     * @param  array  $models
     * @param  string  $relation
     * @return array
     */
    public function initRelation(array $models, $relation)
    {
        foreach ($models as $model) {
            if ($this->isRootRelation) {
                $model->setRelation($relation, $this->related->newCollection());
            } else {
                $payload = $model->payload;
                data_set($payload, $this->path, $this->related->newCollection());
                $model->payload = $payload;
            }
        }

        return $models;
    }

    /**
     * Match the eagerly loaded results to their parents.
     *
     * @param  array  $models
     * @param  \Illuminate\Database\Eloquent\Collection  $results
     * @param  string  $relation
     * @return array
     */
    public function match(array $models, Collection $results, $relation)
    {
        return $this->matchMany($models, $results, $relation);
    }
}
