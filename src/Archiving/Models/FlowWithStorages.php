<?php

namespace Archiving\SDK\Models;

use Archiving\SDK\Models\Flow;
use Archiving\SDK\Traits\EloquentJoin;
use Archiving\SDK\Traits\HasDynamicRelations;

class FlowWithStorages extends Flow
{
    use HasDynamicRelations, EloquentJoin;

    protected $useTableAlias = true;
}
