<?php

namespace Archiving\SDK\Models;

use Archiving\SDK\Models\KlayModel;
use Archiving\SDK\Traits\HasScheme;
use Archiving\SDK\Traits\HasStorageBuilder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Catalog extends KlayModel
{
  use HasScheme,
      HasStorageBuilder,
      SoftDeletes;

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'id';

  /**
   * The model's attributes.
   *
   * @var array
   */
  protected $attributes = [
    'payload' => '[]'
  ];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'id',
    'payload'
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'payload' => 'array',
  ];

  /**
   * The accessors to append to the model's array form.
   *
   * @var array
   */
  protected $appends = [
    'schema'
  ];

  /**
   * The attributes that should be hidden for serialization.
   *
   * @var array
   */
  protected $hidden = [
    'scheme',
    'deleted_at'
  ];

  public function getKeyNameForRelations()
  {
    return $this->getKeyName();
  }

  public function buildCallback($table)
  {
    $table->json('payload');
    $table->timestamps();
    $table->softDeletes();
    $table->engine = 'MyISAM';
  }

  public function update(array $payload = [], $id = null)
  {
    return tap($this->withTrashed()->firstOrNew(['id' => $id]), function ($instance) use ($payload) {

      $instance->validateData($instance->updateArray($instance->payload, $payload));

      if (!$instance->getKey()) {
        $instance->save();
      }

      $instance->updatePayload($payload, function($old_value, &$new_value, $trace_path) {
        $this->findUploadedFiles($old_value, $new_value, $trace_path);
      });

      $instance->deleted_at = null;
      $instance->save();
    });
  }

  public function clear(array $payload = [])
  {
    try {
      $this->validateData($this->clearArray($this->payload, $payload));

      $cleared = [];

      $this->clearPayload($payload, function($old_value, &$new_value, $trace_path) use (&$cleared) {
        $cleared[] = [$old_value, $new_value, $trace_path, true];
      });

      foreach ($cleared as $clear) {
        $this->findUploadedFiles(...$clear);
      }

    } catch (ValidationSchemeException $e) {
      throw  new \Klayware\Exceptions\KlayException("Imposible limpiar. {$e->getMessage()}", 'payload_invalid');
    } catch (\Exception $e) {
      throw  new \Klayware\Exceptions\KlayException($e->getMessage(), 'payload_invalid');
    }

    return $this->save();
  }

  public function replace(array $payload = [])
  {
    try {
      $this->validateData($this->replaceArray($this->payload, $payload));

      $this->replacePayload($payload);

    } catch (ValidationSchemeException $e) {
      throw  new \Klayware\Exceptions\KlayException("Imposible remplazar. {$e->getMessage()}", 'payload_invalid');
    } catch (\Exception $e) {
      throw  new \Klayware\Exceptions\KlayException($e->getMessage(), 'payload_invalid');
    }

    return $this->save();
  }

  public function delete()
  {
    $payload = $this->payload;
    $this->findUploadedFiles(null, $payload, [], true);
    return parent::delete();
  }

  public function validate(array $payload = [], $id = null)
  {
    tap($this->withTrashed()->firstOrNew(['id' => $id]), function ($instance) use ($payload) {
      $instance->validateData($instance->updateArray($instance->payload, $payload));
    });
    return true;
  }

  public function validateClear(array $payload = [], $id = null)
  {
    try {
      tap($this->withTrashed()->firstOrNew(['id' => $id]), function ($instance) use ($payload) {
        $instance->validateData($instance->clearArray($instance->payload, $payload));
      });
    } catch (ValidationSchemeException $e) {
      throw  new \Klayware\Exceptions\KlayException("Imposible limpiar. {$e->getMessage()}", 'payload_invalid');
    } catch (\Exception $e) {
      throw  new \Klayware\Exceptions\KlayException($e->getMessage(), 'payload_invalid');
    }
    return true;
  }

  public function validateReplace(array $payload = [], $id = null)
  {
    try {
      tap($this->withTrashed()->firstOrNew(['id' => $id]), function ($instance) use ($payload) {
        $instance->validateData($instance->replaceArray($instance->payload, $payload));
      });
    } catch (ValidationSchemeException $e) {
      throw  new \Klayware\Exceptions\KlayException("Imposible remplazar. {$e->getMessage()}", 'payload_invalid');
    } catch (\Exception $e) {
      throw  new \Klayware\Exceptions\KlayException($e->getMessage(), 'payload_invalid');
    }
    return true;
  }

  /**
   * Convert the object into something JSON serializable.
   *
   * @return array
   */
  public function jsonSerialize()
  {
      $serialize = $this->toArray();
      $serialize['payload'] = (object) $serialize['payload'];
      return $serialize;
  }
}
