<?php

namespace Archiving\SDK\Models;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Archiving\SDK\Models\KlayModel;

class Scheme extends KlayModel
{
  // use Cachable;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'sys_schemas';

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'type',
    'schema',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'schema' => 'array',
  ];

  /**
   * The attributes that should be hidden for serialization.
   *
   * @var array
   */
  protected $hidden = [
    'deleted_at'
  ];

  /**
   * Dynamically retrieve attributes on the model, or access to scheme tree.
   * @param  string $key
   * @return mixed
   */
  public function __get($key)
  {
    if (in_array($key, array_keys($this->getAttributes()))) {
      return $this->getAttribute($key);
    }
    return array_get($this->getAttribute('schema'), $key, null);
  }

  public function newSchemeType()
  {
    switch ($this->type) {
      case 'cat':
        $scheme = Catalog::setScheme($this);
        break;

      case 'doc':
        $scheme = Document::setScheme($this);
        break;

      case 'flu':
        $scheme = Flow::setScheme($this);
        break;

      default:
        $scheme = null;
        break;
    }
    return $scheme;
  }
}
