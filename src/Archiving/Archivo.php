<?php

namespace Archiving\SDK;

use Archiving\SDK\Client;

class Archivo
{
    const FILE_GET_ENDPOINT = 'archivos:obtener';

    public static function obtener($file_id, Client $cliente)
    {
        $data = [
            'file' => $file_id,
        ];

        try {
            $response = $cliente->performRequest('POST', static::FILE_GET_ENDPOINT, ['body' => $data]);
        } catch (\Exception $e) {
            $response = false;
        }

        return $response;
    }
}
