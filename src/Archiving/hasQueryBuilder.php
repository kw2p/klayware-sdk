<?php

namespace Archiving\SDK;

use Archiving\SDK\QueryBuilder;

Trait hasQueryBuilder
{
    /**
     * Get a new query builder for the model
     *
     * @return \Archiving\SDK\QueryBuilder
     */
    public function newQuery()
    {
        return $this->newQueryBuilder()->setModel($this);
    }

    /**
     * Create a new query builder
     *
     * @return \Archiving\SDK\QueryBuilder
     */
    public function newQueryBuilder()
    {
        return new QueryBuilder;
    }

    /**
     * Handle dynamic method calls into the model.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->newQuery()->{$method}(...$parameters);
    }

    /**
     * Handle dynamic static method calls into the method.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        return (new static)->$method(...$parameters);
    }
}
