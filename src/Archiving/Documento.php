<?php

namespace Archiving\SDK;

use Archiving\SDK\Client;
use Archiving\SDK\Entry;
use Archiving\SDK\Exception;
use Illuminate\Support\Collection;

class Documento extends Entry
{
    const DOCUMENT_All_ENDPOINT = 'documentos:consultar';
    const DOCUMENT_CREATE_ENDPOINT = 'documentos:crear';
    const DOCUMENT_UPDATE_ENDPOINT = 'documentos:actualizar';
    const DOCUMENT_CLEAR_ENDPOINT = 'documentos:limpiar';
    const DOCUMENT_REPLACE_ENDPOINT = 'documentos:remplazar';
    const DOCUMENT_CANCEL_ENDPOINT = 'documentos:cancelar';
    const DOCUMENT_UNCANCEL_ENDPOINT = 'documentos:descancelar';
    const DOCUMENT_COMMENT_ENDPOINT = 'documentos:comentar';
    const DOCUMENT_UNCOMMENT_ENDPOINT = 'documentos:descomentar';
    const DOCUMENT_COMMENTS_ENDPOINT = 'documentos:comentarios';
    const DOCUMENT_EVENTS_ENDPOINT = 'documentos:eventos';
    const DOCUMENT_VALID_ENDPOINT = 'documentos:validar';
    const DOCUMENT_VALID_CLEAR_ENDPOINT = 'documentos:validar_limpiar';
    const DOCUMENT_VALID_REPLACE_ENDPOINT = 'documentos:validar_remplazar';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'folio';

    public static function consultar($schema, Client $cliente, $params = [], &$paginate = null)
    {
        $data = array_filter([
            'schema' => $schema,
            'with' => $params['with'] ?? null,
            'with-storage' => $params['with-storage'] ?? null,
            'filter' => $params['filter'] ?? null,
            'order' => $params['order'] ?? null,
            'orderByJoin' => $params['orderByJoin'] ?? null,
            'limit' => $params['limit'] ?? null,
            'page' => $params['page'] ?? null,
        ]);

        $response = $cliente->performJsonRequest('POST', static::DOCUMENT_All_ENDPOINT, ['body' => $data]);

        $collection = (new static($schema, $cliente))->createCollectionFromResponse($response);

        $paginate = $response['paginate'] ?? $paginate;

        return $collection;
    }

    public static function cargar($schema, string $folio, $serie, Client $cliente, array $extra_params = [])
    {
        $entry = static::consultar($schema, $cliente, array_only(array_filter($extra_params), ["with", "with-storage"]) + ['filter' => ['where' => [
            ['field' => 'folio', 'operator' => '=', 'value' => $folio],
            ['field' => 'serie', 'operator' => '=', 'value' => $serie]
        ]]])->first();

        if ($entry === null) {
            (new static($schema, $cliente))->throwNotFoundException("schema: {$schema}, documento: {$folio}, serie: {$serie}");
        }

        return $entry;
    }

    public function crear($serie, $payload = null, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'serie' => $serie,
            'payload' => $payload,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_CREATE_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function actualizar(array $payload, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'payload' => $payload,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_UPDATE_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function limpiar(array $payload, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'payload' => $payload,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_CLEAR_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function remplazar(array $payload, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'payload' => $payload,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_REPLACE_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function cancelar($author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_CANCEL_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function descancelar($author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_UNCANCEL_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function comentar($comentario, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'comentario' => $comentario,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_COMMENT_ENDPOINT, ['body' => $data]);

        return $this->createRelationFromResponse('comentarios', $response);
    }

    public function descomentar($comentario_id, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'comentario_id' => $comentario_id,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_UNCOMMENT_ENDPOINT, ['body' => $data]);

        return $this->ifSuccessResponse($response, function($response) use ($comentario_id) {
            return $this->removeItemFromRelation('comentarios', $comentario_id);
        });
    }


    public function comentarios()
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => (string) $this->folio,
            'serie' => $this->serie,
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_COMMENTS_ENDPOINT, ['body' => $data]);

        return $this->createRelationsFromResponse('comentarios', $response);
    }

    public function eventos()
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => (string) $this->folio,
            'serie' => $this->serie,
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_EVENTS_ENDPOINT, ['body' => $data]);

        return $this->createRelationsFromResponse('eventos', $response);
    }

    public static function consultarEventos(Client $cliente, $params = [], &$paginate = null)
    {
        $data = array_filter([
            'filter' => $params['filter'] ?? null,
            'order' => $params['order'] ?? null,
            'limit' => $params['limit'] ?? null,
            'page' => $params['page'] ?? null,
        ]);

        $response = $cliente->performJsonRequest('POST', static::DOCUMENT_EVENTS_ENDPOINT, ['body' => $data]);

        $collection = new Collection($response['data'] ?? []);

        $paginate = $response['paginate'] ?? $paginate;

        return $collection;
    }

    public function validarSchema(array $payload)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'payload' => $payload
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_VALID_ENDPOINT, ['body' => $data]);

        return $this->ifSuccessResponse($response, function($response) {
            return $response;
        });
    }

    public function validarLimpiarSchema(array $payload)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'payload' => $payload
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_VALID_CLEAR_ENDPOINT, ['body' => $data]);

        return $this->ifSuccessResponse($response, function($response) {
            return $response;
        });
    }

    public function validarRemplazarSchema(array $payload)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'payload' => $payload
        ]);

        $response = $this->service->performJsonRequest('POST', static::DOCUMENT_VALID_REPLACE_ENDPOINT, ['body' => $data]);

        return $this->ifSuccessResponse($response, function($response) {
            return $response;
        });
    }

    public function getQueryEndpoint()
    {
        return static::DOCUMENT_All_ENDPOINT;
    }

    public function throwNotFoundException($params)
    {
        throw new Exception("Documento [{$params}] no existe", 'archiving.actions.documents.load.document_nonexist');
    }
}
