<?php

namespace Archiving\SDK;

use Exception as ExceptionBase;
use Throwable;

class Exception extends ExceptionBase
{
    function __construct(string $message , $code , Throwable $previous = NULL) {
        $this->message = $message;
        $this->code = $code;
    }

    function toArray($params = null) {
        return [
            'status' => 'fail',
            'message' => $this->message,
            'code' => $this->code,
        ] + ($params ? ['params' => $params] : []);
    }
}
