<?php

namespace Archiving\SDK;

use Archiving\SDK\Client;
use Archiving\SDK\Entry;
use Archiving\SDK\Exception;
use Illuminate\Support\Collection;

class Flujo extends Entry
{
    const FLOW_All_ENDPOINT = 'flujos:consultar';
    const FLOW_CREATE_ENDPOINT = 'flujos:crear';
    const FLOW_UPDATE_ENDPOINT = 'flujos:actualizar';
    const FLOW_CLEAR_ENDPOINT = 'flujos:limpiar';
    const FLOW_REPLACE_ENDPOINT = 'flujos:remplazar';
    const FLOW_ADVANCE_ENDPOINT = 'flujos:avanzar';
    const FLOW_RETREAT_ENDPOINT = 'flujos:retroceder';
    const FLOW_OPEN_ENDPOINT = 'flujos:abrir';
    const FLOW_CLOSE_ENDPOINT = 'flujos:cerrar';
    const FLOW_CANCEL_ENDPOINT = 'flujos:cancelar';
    const FLOW_UNCANCEL_ENDPOINT = 'flujos:descancelar';
    const FLOW_COMMENT_ENDPOINT = 'flujos:comentar';
    const FLOW_UNCOMMENT_ENDPOINT = 'flujos:descomentar';
    const FLOW_COMMENTS_ENDPOINT = 'flujos:comentarios';
    const FLOW_EVENTS_ENDPOINT = 'flujos:eventos';
    const FLOW_VALID_ENDPOINT = 'flujos:validar';
    const FLOW_VALID_CLEAR_ENDPOINT = 'flujos:validar_limpiar';
    const FLOW_VALID_REPLACE_ENDPOINT = 'flujos:validar_remplazar';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'folio';

    public static function consultar($schema, Client $cliente, $params = [], &$paginate = null)
    {
        $data = array_filter([
            'schema' => $schema,
            'with' => $params['with'] ?? null,
            'with-storage' => $params['with-storage'] ?? null,
            'filter' => $params['filter'] ?? null,
            'order' => $params['order'] ?? null,
            'orderByJoin' => $params['orderByJoin'] ?? null,
            'limit' => $params['limit'] ?? null,
            'page' => $params['page'] ?? null,
        ]);

        $response = $cliente->performJsonRequest('POST', static::FLOW_All_ENDPOINT, ['body' => $data]);

        $collection = (new static($schema, $cliente))->createCollectionFromResponse($response);

        $paginate = $response['paginate'] ?? $paginate;

        return $collection;
    }

    public static function cargar($schema, string $folio, $serie, Client $cliente, array $extra_params = [])
    {
        $entry = static::consultar($schema, $cliente, array_only(array_filter($extra_params), ["with", "with-storage"]) + ['filter' => ['where' => [
            ['field' => 'folio', 'operator' => '=', 'value' => $folio],
            ['field' => 'serie', 'operator' => '=', 'value' => $serie]
        ]]])->first();

        if ($entry === null) {
            (new static($schema, $cliente))->throwNotFoundException("schema: {$schema}, folio: {$folio}, serie: {$serie}");
        }

        return $entry;
    }

    public function crear($etapa, $serie, $payload = null, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'etapa' => $etapa,
            'serie' => $serie,
            'payload' => $payload,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_CREATE_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function actualizar(array $payload, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'payload' => $payload,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_UPDATE_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function limpiar(array $payload, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'payload' => $payload,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_CLEAR_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function remplazar(array $payload, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'payload' => $payload,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_REPLACE_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function avanzar($etapa, $payload = null, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'etapa' => $etapa,
            'payload' => $payload,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_ADVANCE_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function retroceder($author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_RETREAT_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function abrir($author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_OPEN_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function cerrar($author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_CLOSE_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function cancelar($author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_CANCEL_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function descancelar($author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_UNCANCEL_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function comentar($comentario, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'comentario' => $comentario,
            'author' => $author,
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_COMMENT_ENDPOINT, ['body' => $data]);

        return $this->createRelationFromResponse('comentarios', $response);
    }

    public function descomentar($comentario_id, $author = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'comentario_id' => $comentario_id,
            'author' => $author
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_UNCOMMENT_ENDPOINT, ['body' => $data]);

        return $this->ifSuccessResponse($response, function($response) use ($comentario_id) {
            return $this->removeItemFromRelation('comentarios', $comentario_id);
        });
    }

    public function comentarios()
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => (string) $this->folio,
            'serie' => $this->serie,
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_COMMENTS_ENDPOINT, ['body' => $data]);

        return $this->createRelationsFromResponse('comentarios', $response);
    }

    public function eventos()
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => (string) $this->folio,
            'serie' => $this->serie,
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_EVENTS_ENDPOINT, ['body' => $data]);

        return $this->createRelationsFromResponse('eventos', $response);
    }

    public function validarSchema(array $payload)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'payload' => $payload
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_VALID_ENDPOINT, ['body' => $data]);

        return $this->ifSuccessResponse($response, function($response) {
            return $response;
        });
    }

    public function validarLimpiarSchema(array $payload)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'payload' => $payload
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_VALID_CLEAR_ENDPOINT, ['body' => $data]);

        return $this->ifSuccessResponse($response, function($response) {
            return $response;
        });
    }

    public function validarRemplazarSchema(array $payload)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'folio' => $this->folio,
            'serie' => $this->serie,
            'payload' => $payload
        ]);

        $response = $this->service->performJsonRequest('POST', static::FLOW_VALID_REPLACE_ENDPOINT, ['body' => $data]);

        return $this->ifSuccessResponse($response, function($response) {
            return $response;
        });
    }

    public function getQueryEndpoint()
    {
        return static::FLOW_All_ENDPOINT;
    }

    public function throwNotFoundException($params)
    {
        throw new Exception("Flujo [{$params}] no existe", 'archiving.actions.flows.load.flow_nonexist');
    }
}
