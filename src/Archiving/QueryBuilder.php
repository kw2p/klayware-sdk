<?php

namespace Archiving\SDK;

use Archiving\SDK\Entry;
use Archiving\SDK\Exception;
use ArrayAccess;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Arr;
use JsonSerializable;

class QueryBuilder
{
    protected $path = 'filter';

    /**
     * The base query builder instance.
     *
     * @var array
     */
    protected $query;

    /**
     * The model being queried.
     *
     * @var \Archiving\SDK\Entry
     */
    protected $model;

    /**
     * Get the model instance being queried.
     *
     * @return \Archiving\SDK\Entry|static
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set a model instance for the model being queried.
     *
     * @param  $model
     * @return $this
     */
    public function setModel(Entry $model)
    {
        $this->model = $model;

        $this->query['schema'] = $model->getAttribute('schema');

        return $this;
    }

    public function setSchema($schema)
    {
        $this->getModel()->setAttribute('schema', $schema);

        $this->query['schema'] = $schema;

        return $this;
    }

    public function with($relations)
    {
        $path = $this->getPath();
        $this->setPath(null);

        $this->mergeQuery('with', $relations);

        $this->setPath($path);

        return $this;
    }

    public function withStorage($relations)
    {
        $path = $this->getPath();
        $this->setPath(null);

        $this->mergeQuery('with-storage', $relations);

        $this->setPath($path);

        return $this;
    }

    public function withTrashed()
    {
        $path = $this->getPath();
        $this->setPath(null);

        $this->mergeQuery('with-trashed', true);

        $this->setPath($path);

        return $this;
    }

    public function where($filter, $operatorOrValue = null, $value = null, $collate = false)
    {
        if ($filter instanceof \Closure) {
            return $this->and($filter);
        }

        $struct = $value !== null ? [
            'operator' => $operatorOrValue,
            'value' => $value,
            'collate' => $collate
        ] : $operatorOrValue;

        $filter = $this->wrap($filter, $struct);

        $where = $this->reduce($filter, function($acc, $filter) {
            return array_merge($acc, $this->parseFilters('where', $filter));
        }, []);

        $this->mergeQuery('.where', $where);

        return $this;
    }

    public function whereFirst($filter, $operatorOrValue = null, $value = null, $collate = false)
    {
        return $this->where($filter, $operatorOrValue, $value, $collate)->first();
    }

    /**
     * Add a where clause on the primary key to the query.
     *
     * @param  mixed  $id
     * @return $this
     */
    public function whereKey($idOrFolio)
    {
        if (is_array($idOrFolio) || $idOrFolio instanceof Arrayable) {
            $this->whereIn($this->model->getKeyName(), $idOrFolio);

            return $this;
        }

        return $this->where($this->model->getKeyName(), $idOrFolio);
    }

    public function orWhere($filter, $operatorOrValue = null, $value = null, $collate = false)
    {
        if ($filter instanceof \Closure) {
            return $this->or($filter);
        }

        $struct = $value !== null ? [
            'operator' => $operatorOrValue,
            'value' => $value,
            'collate' => $collate
        ] : $operatorOrValue;

        $filter = $this->wrap($filter, $struct);

        $orWhere = $this->reduce($filter, function($acc, $filter) {
            return array_merge($acc, $this->parseFilters('orwhere', $filter));
        }, []);

        $this->mergeQuery('.orWhere', $orWhere);

        return $this;
    }

    public function whereIn($filter, $value = null)
    {
        $filter = $this->wrap($filter, $value);

        $whereIn = $this->reduce($filter, function($acc, $filter) {
            return array_merge($acc, $this->parseFilters('wherein', $filter));
        }, []);

        $this->mergeQuery('.whereIn', $whereIn);

        return $this;
    }

    public function orWhereIn($filter, $value = null)
    {
        $filter = $this->wrap($filter, $value);

        $orWhereIn = $this->reduce($filter, function($acc, $filter) {
            return array_merge($acc, $this->parseFilters('orwherein', $filter));
        }, []);

        $this->mergeQuery('.orWhereIn', $orWhereIn);

        return $this;
    }

    public function whereNull($filter)
    {
        $filter = array_wrap($filter);

        $whereIn = $this->parseFilters('whereNull', $filter);

        $this->mergeQuery('.whereNull', $whereIn);

        return $this;
    }

    public function whereNotNull($filter)
    {
        $filter = array_wrap($filter);

        $whereIn = $this->parseFilters('whereNotNull', $filter);

        $this->mergeQuery('.whereNotNull', $whereIn);

        return $this;
    }

    public function whereJsonContains($filter, $value = null)
    {
        if ($filter instanceof \Closure) {
            return $this->and($filter);
        }

        $filter = $this->wrap($filter, $value);

        $whereJsonContains = $this->reduce($filter, function($acc, $filter) {
            return array_merge($acc, $this->parseFilters('whereJsonContains', $filter));
        }, []);

        $this->mergeQuery('.whereJsonContains', $whereJsonContains);

        return $this;
    }

    public function orWhereJsonContains($filter, $value = null)
    {
        if ($filter instanceof \Closure) {
            return $this->and($filter);
        }

        $filter = $this->wrap($filter, $value);

        $orWhereJsonContains = $this->reduce($filter, function($acc, $filter) {
            return array_merge($acc, $this->parseFilters('orWhereJsonContains', $filter));
        }, []);

        $this->mergeQuery('.orWhereJsonContains', $orWhereJsonContains);

        return $this;
    }


    public function whereJsonDoesntContain($filter, $value = null)
    {
        if ($filter instanceof \Closure) {
            return $this->and($filter);
        }

        $filter = $this->wrap($filter, $value);

        $whereJsonDoesntContain = $this->reduce($filter, function($acc, $filter) {
            return array_merge($acc, $this->parseFilters('whereJsonDoesntContain', $filter));
        }, []);

        $this->mergeQuery('.whereJsonDoesntContain', $whereJsonDoesntContain);

        return $this;
    }

    public function orWhereJsonDoesntContain($filter, $value = null)
    {
        if ($filter instanceof \Closure) {
            return $this->and($filter);
        }

        $filter = $this->wrap($filter, $value);

        $orWhereJsonDoesntContain = $this->reduce($filter, function($acc, $filter) {
            return array_merge($acc, $this->parseFilters('orWhereJsonDoesntContain', $filter));
        }, []);

        $this->mergeQuery('.orWhereJsonDoesntContain', $orWhereJsonDoesntContain);

        return $this;
    }

    public function whereJsonLength($filter, $operatorOrValue = null, $value = null)
    {
        if ($filter instanceof \Closure) {
            return $this->and($filter);
        }

        $struct = $value !== null ? [
            'operator' => $operatorOrValue,
            'value' => $value,
        ] : $operatorOrValue;

        $filter = $this->wrap($filter, $struct);

        $whereJsonLength = $this->reduce($filter, function($acc, $filter) {
            return array_merge($acc, $this->parseFilters('whereJsonLength', $filter));
        }, []);

        $this->mergeQuery('.whereJsonLength', $whereJsonLength);

        return $this;
    }

    public function orWhereJsonLength($filter, $operatorOrValue = null, $value = null)
    {
        if ($filter instanceof \Closure) {
            return $this->and($filter);
        }

        $struct = $value !== null ? [
            'operator' => $operatorOrValue,
            'value' => $value,
        ] : $operatorOrValue;

        $filter = $this->wrap($filter, $struct);

        $orWhereJsonLength = $this->reduce($filter, function($acc, $filter) {
            return array_merge($acc, $this->parseFilters('orWhereJsonLength', $filter));
        }, []);

        $this->mergeQuery('.orWhereJsonLength', $orWhereJsonLength);

        return $this;
    }

    public function orderBy($order, $direction = 'asc')
    {
        $order = $this->wrap($order, $direction);

        $orderBy = $this->reduce($order, function($acc, $order) {
            return array_merge($acc, $this->parseFilters('order', $order));
        }, []);

        $path = $this->getPath();
        $this->setPath(null);

        $this->mergeQuery('order', $orderBy);

        $this->setPath($path);

        return $this;
    }

    /**
     * Set the "limit" value of the query.
     *
     * @param  int  $limit
     * @return $this
     */
    public function limit($limit)
    {
        if ($limit >= 0) {
            $this->query['limit'] = $limit;
        }

        return $this;
    }

    public function find($idOrFolio)
    {
        if (is_array($idOrFolio) || $idOrFolio instanceof Arrayable) {
            return $this->findMany($idOrFolio);
        }

        return $this->whereKey($idOrFolio)->first();
    }

    public function findMany($idsOrFolios)
    {
        if (empty($idsOrFolios)) {
            return $this->model->newCollection();
        }

        return $this->whereKey($idsOrFolios)->get();
    }

    public function findOrFail($idOrFolio)
    {
        $result = $this->find($idOrFolio);

        if (is_array($idOrFolio)) {
            if (count($result) === count(array_unique($idOrFolio))) {
                return $result;
            }
            $idOrFolio = array_diff(Arr::wrap($idOrFolio), $result->pluck('id')->toArray());
        } elseif (! is_null($result)) {
            return $result;
        }
        $idOrFolio = Arr::wrap($idOrFolio);
        $this->model->throwNotFoundException($this->debugFilter(reset($idOrFolio)));
    }

    public function first()
    {
        return $this->limit(1)->get()->first();
    }

    public function firstOrFail()
    {
        $result = $this->first();

        if ($result != null) {
            return $result;
        }

        $this->model->throwNotFoundException($this->debugFilter());
    }

    public function all()
    {
        return $this->model->newQuery()->get();
    }

    public function get()
    {
        $this->query['skipPaginated'] = true;

        $response = $this->model->getServiceClient()->performJsonRequest('POST', $this->model->getQueryEndpoint(), [
            'body' => $this->query
        ]);
        return $this->model->createCollectionFromResponse($response);
    }

    public function and($closure)
    {
        $path = $this->getPath();
        $this->setPath($path . '.and');

        $closure($this);

        $this->setPath($path);
        return $this;
    }

    public function or($closure)
    {
        $path = $this->getPath();
        $this->setPath($path . '.or');

        $closure($this);

        $this->setPath($path);
        return $this;
    }

    private function mergeQuery($path, $merge)
    {
        $path = $this->path . $path;
        data_set($this->query, $path, array_merge(data_get($this->query, $path, []), $merge));
    }

    private function parseFilters($type, $filters)
    {
        if ($type === 'where' || $type === 'orwhere' ) {

            if (is_indexed($filters)) {
                $filters = [
                    $filters[0] => ($filters[2] ?? null) ? [
                        'operator' => $filters[1],
                        'value' => $filters[2],
                        'collate' => $filters[3] ?? false
                    ] : $filters[1]
                ];
            }

            return $this->reduce(array_keys($filters), function($acc, $field) use ($filters) {
                $item = is_array($filters[$field]) ? $filters[$field] : [
                    'value' => $filters[$field]
                ];

                $acc[] = [
                    'field' => $field,
                    'operator' => $item['operator'] ?? '=',
                    'value' => $item['value'],
                    'collate' => $item['collate'] ?? false,
                ];

                return $acc;
            }, []);
        }

        if ($type === 'wherein' || $type === 'orwherein') {

            if (is_indexed($filters)) {
                $filters = [
                    $filters[0] => $filters[1]
                ];
            }

            return $this->reduce(array_keys($filters), function($acc, $field) use ($filters) {
                $acc[] = [
                    'field' => $field,
                    'value' => $filters[$field],
                ];

                return $acc;
            }, []);
        }

        if (in_array($type, ['whereJsonContains', 'orWhereJsonContains', 'whereJsonDoesntContain', 'orWhereJsonDoesntContain'])) {

            if (is_indexed($filters)) {
                $filters = [
                    $filters[0] => $filters[1]
                ];
            }

            return $this->reduce(array_keys($filters), function($acc, $field) use ($filters) {
                $acc[] = [
                    'field' => $field,
                    'value' => $filters[$field],
                ];

                return $acc;
            }, []);
        }

        if ($type === 'whereJsonLength' || $type === 'orWhereJsonLength' ) {

            if (is_indexed($filters)) {
                $filters = [
                    $filters[0] => ($filters[2] ?? null) ? [
                        'operator' => $filters[1],
                        'value' => $filters[2],
                    ] : $filters[1]
                ];
            }

            return $this->reduce(array_keys($filters), function($acc, $field) use ($filters) {
                $item = is_array($filters[$field]) ? $filters[$field] : [
                    'value' => $filters[$field]
                ];

                $acc[] = [
                    'field' => $field,
                    'operator' => $item['operator'] ?? '=',
                    'value' => $item['value'],
                ];

                return $acc;
            }, []);
        }

        if ($type === 'whereNull' || $type === 'whereNotNull') {
            return $this->reduce($filters, function($acc, $field) use ($filters) {
                $acc[] = [
                    'field' => $field,
                ];
                return $acc;
            }, []);
        }

        if ($type === 'order') {
            return $this->reduce(array_keys($filters), function($acc, $field) use ($filters) {
                $acc[] = [
                    'field' => $field,
                    'direction' => $filters[$field],
                ];

                return $acc;
            }, []);
        }

        return null;
    }

    function reduce(iterable $iterable, callable $function, $startValue = null) {
        $acc = $startValue;
        foreach ($iterable as $key => $value) {
            $acc = $function($acc, $value, $key);
        }
        return $acc;
    }

    private function wrap($key, $value)
    {
        return is_array($key) ? (is_indexed($key) ? $key : [$key]) : [array_column([
            [$key, $value]
        ], 1, 0)];
    }

    public function dumpQuery() {
        return $this->query;
    }

    private function debugFilter($overload_value = null) {

        $debug = [
            "schema: {$this->query['schema']}"
        ];

        foreach ($this->query['filter'] ?? [] as $key => $filter) {
            foreach ($filter as $n) {
                $field = $n['field'];
                $value = $overload_value ?? $n['value'] ?? '\'\'';
                $value = is_array($value) ? implode(',', $value) : $value;
                $debug[] = "{$key}.{$field}: {$value}";
                if ($overload_value) break 2;
            }
        }

        return implode(', ', $debug);
    }

    function getPath()
    {
        return $this->path;
    }

    function setPath($path)
    {
        $this->path = $path;
    }

}
