<?php

namespace Archiving\SDK\Traits;

trait MemoryStorage
{
    protected static $memoryStorage = [];

    final protected function memoize($key, callable $callback)
    {
        if (is_array($key)) {
            $key = implode('-', $key);
        }
        if (!array_key_exists($key, static::$memoryStorage)) {
          static::$memoryStorage[$key] = $callback();
        }
        return static::$memoryStorage[$key];
    }
}
