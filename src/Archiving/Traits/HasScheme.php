<?php

namespace Archiving\SDK\Traits;

use Archiving\Exceptions\ValidationSchemeException;
use Archiving\Formats\MysqlDateTime;
use Archiving\SDK\Models\MemoryStorageV2;
use Archiving\SDK\Models\Scheme;
use Archiving\SDK\Traits\MemoryStorage;
use Opis\JsonSchema\FormatContainer;
use Opis\JsonSchema\Validator;

trait HasScheme
{
  use MemoryStorage;

  /**
   * Set scheme relation to the model
   *
   * @param Scheme $scheme
   *
   * @return $this
   */
  public static function setScheme(Scheme $scheme)
  {
    return tap(new static, function ($schemeble) use ($scheme) {
      $schemeble->setTable("{$scheme->type}_{$scheme->name}");
      $schemeble->setRelation('scheme', $scheme);
    });
  }

  public function getSchemeAttribute()
  {
    if ($this->relationLoaded('scheme')) {
      return $this->getRelation('scheme');
    }

    if (preg_match('/^(?<type>[^_]+)_(?<schema>.*)$/', $this->getTable(), $match)) {
      $scheme = (new MemoryStorageV2)->memoize(['@scheme', $match['type'], $match['schema']], function() use ($match) {
        return Scheme::where(['name' => $match['schema'], 'type' => $match['type']])->first();
      });
      $this->setRelation('scheme', $scheme);
      return $scheme;
    }
  }

  public function getSchemaAttribute()
  {
    return $this->scheme->name;
  }

  public function getPropertyScheme($array_path)
  {
    $scheme = $this->scheme->payload;
    foreach ($array_path as $key => $value) {
      if (is_numeric($value)) {
        if ($scheme['type'] == 'object') {
          $scheme = $scheme['properties'];
        }
        elseif ($scheme['type'] == 'array') {
          $scheme = $scheme['items'] ?? null;
        }
        continue;
      }
      elseif (@$scheme[$value]) {
        $scheme = $scheme[$value];
      }
      elseif (@$scheme['properties'][$value]) {
        $scheme = $scheme['properties'][$value];
      }
      else {
        $scheme = null;
        break;
      }
    }
    return $scheme;
  }

  public function hasContentMediaType($scheme)
  {
    if (isset($scheme['anyOf'])) {
      foreach ($scheme['anyOf'] as $sub) {
        if ($sub['contentMediaType'] ?? false) return true;
      }
    }

    return $scheme['contentMediaType'] ?? false;
  }

  public function validateData($payload, $onlyPresentStorages = false)
  {
    $scheme = $this->scheme->schema['payload'];

    if ($onlyPresentStorages) {
      $scheme['properties'] = (object) array_intersect_key($scheme['properties'], $payload);
    }

    $scheme = json_decode(json_encode((object) $scheme));
    $object = json_decode(json_encode((object) $payload));

    $validator = new Validator;

    $validator->setFormats((new FormatContainer)->add('string', 'mysql-date-time', new MysqlDateTime));

    #
    validate:

    try {
        $validation = $validator->dataValidation($object, $scheme);
    } catch (\Exception $e) {
        throw new ValidationSchemeException($e->getMessage(), 'schema_validation_invalid');
    }

    if ($validation->hasErrors()) {
      $error = $validation->getFirstError();
      $keyword = str_replace('$', '', $error->keyword());

      if ($keyword == 'type') {
        # Fix: {} => []
        if ($error->keywordArgs() == ["expected" => "object", "used" => "array"] && empty($error->data())) {
          $path = implode('.', $error->dataPointer());
          data_set($object, $path, (object) []);
          goto validate;
        }
      }

      if ($keyword == 'anyOf') {
        foreach ($error->suberrors() as $suberror) {
          # Fix: "" => null
          if ($suberror->keywordArgs() == ['expected' => 'null', 'used' => 'string'] && $error->data() === '') {
            $subpath = implode('.', $suberror->dataPointer());
            data_set($object, $subpath, null);
            goto validate;
          }
        }
      }

      if ($keyword != 'contentEncoding') {
        $this->{"{$keyword}Exception"}($error->schema(), $error->data(), $error->dataPointer(), $error->keywordArgs(), $error->subErrors());
      }
    }
  }

  private function schemaException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function typeException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function constException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function enumException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function notException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function thenException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function elseException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function anyOfException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function oneOfException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function allOfException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function filtersException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function formatException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function minLengthException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function maxLengthException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function patternException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function contentEncodingException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function contentMediaTypeException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function minimumException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function exclusiveMinimumException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function exclusiveMaximumException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function maximumException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function multipleOfException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function minItemsException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("No hay suficientes elementos en «{$field}», requerido (s): {$keywords['min']}", 'payload_invalid');
  }

  private function maxItemsException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function uniqueItemsException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function containsException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function requiredException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->properties->{$keywords['missing']}->title ?? $keywords['missing'];
    throw new ValidationSchemeException("La propiedad {$field} es requerida.", 'payload_invalid');
  }

  private function dependenciesException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function minPropertiesException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function maxPropertiesException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function propertyNamesException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function patternPropertiesException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $field = $scheme->title ?? end($data_path);
    throw new ValidationSchemeException("El campo {$field} es invalido.", 'payload_invalid');
  }

  private function additionalPropertiesException($scheme, $data, $data_path, $keywords, $suberrors = [])
  {
    $sub_errors_data_path = $suberrors[0]->dataPointer();
    $field = end($sub_errors_data_path);
    throw new ValidationSchemeException("Propiedades adicionales no permitidas: {$field}", 'payload_invalid');
  }
}
