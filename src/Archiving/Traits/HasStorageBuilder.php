<?php

namespace Archiving\SDK\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

trait HasStorageBuilder
{
  public function build($self)
  {
    Schema::create($this->getTable(), function (Blueprint $table) use ($self) {
      $table->increments('id');
      $self->buildCallback($table);
    });
    $self->buildAfter();
  }

  public function buildCallback($table)
  {
    $table->timestamps();
    $table->engine = 'MyISAM';
  }

  public function buildAfter()
  {
  }

  /**
   * Create a new Eloquent query builder for the model.
   *
   * @param  \Illuminate\Database\Query\Builder  $query
   * @return \Illuminate\Database\Eloquent\Builder|static
   */
  public function newEloquentBuilder($query)
  {
    return new class($query) extends Builder {

      function get($columns = ['*']) {
        return $this->buildStorage(function() use ($columns) {
          return parent::get($columns);
        });
      }

      function insertGetId(...$parameters) {
        $fn = __FUNCTION__;
        return $this->buildStorage(function() use ($fn, $parameters) {
          return $this->toBase()->{$fn}(...$parameters);
        });
      }

      function paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null) {
        return $this->buildStorage(function() use ($perPage, $columns, $pageName, $page) {
          return parent::paginate($perPage, $columns, $pageName, $page);
        });
      }

      function buildStorage(\Closure $closure) {
        try {
          return $closure();
        } catch (\Illuminate\Database\QueryException $e) {
          if ($e->getCode() == '42S02') {
            $this->model->build($this->model);
            return $closure();
          } else {
            throw new $e($e->getMessage(), $e->getBindings(), $e->getPrevious());
          }
        }
      }
    };
  }
}
