<?php

namespace Archiving\SDK\Traits;

use Archiving\SDK\Models\MemoryStorageV2;
use Archiving\SDK\Models\Relations\HasMany;
use Archiving\SDK\Models\Relations\HasOne;
use Archiving\SDK\Models\Scheme;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Klayware\Exceptions\KlayException;

trait HasDynamicRelations
{
    /**
     * The name of the storage in the scheme
     * @var string
     */
    protected $storageName = 'payload';

    /**
     * Relations to be load dynamically
     * @var array
     */
    protected $withRootRelations = [];

    /**
     * Relations to be load dynamically
     * @var array
     */
    protected $withStorageRelations = [];

    /**
     * Store the relations
     *
     * @var array
     */
    public static $dynamicRelations = [];

    /**
     * Add a new relation
     *
     * @param $name
     * @param $closure
     */
    public static function addDynamicRelation($name, $closure)
    {
        static::$dynamicRelations[$name] = $closure;
    }

    /**
     * Determine if a relation exists in dynamic relationships list
     *
     * @param $name
     *
     * @return bool
     */
    public static function hasDynamicRelation($name)
    {
        return isset(static::$dynamicRelations[$name]);
    }

    /**
     * Determine if a get mutator exists for an attribute.
     *
     * @param  string  $key
     * @return bool
     */
    public function hasGetMutator($key)
    {
        return parent::hasGetMutator($key) || static::hasDynamicRelation('get'.Str::studly($key).'Attribute');
    }

    /**
     * Get an attribute from the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function getAttribute($key)
    {
        if (is_array($key)) { //Check for multi-columns relationship
            return array_map(function ($k) {
                return parent::getAttribute($k);
            }, $key);
        }
        return parent::getAttribute($key);
    }

    /**
     * If the method exists in relations then
     * return the relation or else
     * return the call to the parent
     *
     * @param $name
     * @param $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if (static::hasDynamicRelation($name)) {
            return (static::$dynamicRelations[$name])->bindTo($this, static::class)($arguments);
        }

        return parent::__call($name, $arguments);
    }

    public function getStorage()
    {
        return $this->scheme->schema[$this->storageName]['properties'] ?? $this->scheme->schema[$this->storageName];
    }

    public function setGetterAttribute($localKey)
    {
        if (Str::contains($localKey, '->')) {
            static::addDynamicRelation('get'.Str::studly($localKey).'Attribute', function() use ($localKey) {
                [$key, $path] = explode('->', $localKey, 2);
                $path = str_replace('->', '.', $path);
                return data_get($this->getAttribute($key), $path);
            });
        }
        return $localKey;
    }

    public function localKeysResolve($localKeys)
    {
        if (is_array($localKeys)) {
            foreach ($localKeys as $index => $localKey) {
                $localKeys[$index] = $this->setGetterAttribute($localKey);
            }
        } else {
            $localKeys = $this->setGetterAttribute($localKeys);
        }
        return $localKeys;
    }

    /**
    * Get the storage relations definition
    * @param  array $relations
    * @return array
    */
    public function getRootRelations(array $with)
    {
        $relations = [];

        foreach ($this->scheme->schema['relations'] ?? [] as $relationName => $relation) {

            # Si relaciona a documento/flujo
            $isCompositeKey = $relation['related']['type'] != 'cat';

            $hasManyType = $relation['type'] == 'hasMany' ? 'object' : null;
            $hasOneType = $relation['type'] == 'hasOne' ? 'object' : null;

            $relations[$relationName] = [
                'extra' => [
                    'path' => null,
                    'isCompositeKey' => $isCompositeKey,
                    'hasManyType' => $hasManyType,
                    'hasOneType' => $hasOneType,
                ]
            ] + $relation;
        }

        if (in_array('*', $with)) {
            return $relations;
        }

        return array_intersect_key($relations, array_flip($with));
    }

    public function withRootRelations(array $with)
    {
        $relations = $this->getRootRelations($with);
        foreach ($relations as $name => $relation) {
            $this->withRootRelations[] = $name;
            $this->setUpRelation($name, $relation);
        }
        return $this;
    }

    /**
     * Get the storage relations definition
     * @return array
     */
    public function getStorageRelations($withStorage)
    {
        $relations = [];

        foreach ($this->getStorage() as $subStorageName => $subStorage) {
            foreach ($subStorage['relations'] ?? [] as $relationName => $relation) {

                # Si relaciona a documento/flujo
                $isCompositeKey = $relation['related']['type'] != 'cat';

                # BpOf: array_wrap
                $path = (array) ($subStorageName ?: null);

                # BpOf: array_wrap
                $localKeys = (array) $relation['localKey'];
                foreach ($localKeys as $indexKey => $localKey) {
                    # BpOf: implode
                    foreach ($path + ['n' => 'payload'] as $segment)  {
                        $localKeys[$indexKey] = $segment . '->' . $localKeys[$indexKey];
                    }
                }

                # BpOf: implode
                $dotpath = '';
                foreach ($path as $segment) $dotpath = $segment . '.' . $dotpath;

                $hasManyType = $relation['type'] == 'hasMany' ? $subStorage['type'] : null;
                $hasOneType = $relation['type'] == 'hasOne' ? $subStorage['type'] : null;

                $relations[$relationName] = [
                    # BpOf: count - si no existe indice 1, pasamos string de lo contrario array
                    'localKey' => $localKeys[1] ?? false ? $localKeys : $localKeys[0],
                    'extra' => [
                        'path' => $dotpath . (($hasOneType ?? $hasManyType) == 'array' ? '*.' : '') . '_relaciones.' . $relationName . ($hasManyType == 'object' ? '.*' : ''),
                        'isCompositeKey' => $isCompositeKey,
                        'hasManyType' => $hasManyType,
                        'hasOneType' => $hasOneType
                    ]
                ] + $relation;
            }
        }

        if (in_array('*', $withStorage)) {
            return $relations;
        }

        uksort($relations, function($k, $v) use ($withStorage) {
          return array_search($k, $withStorage);
        });

        return array_intersect_key($relations, array_flip($withStorage));
    }

    /**
     * Set the storage relations
     * @param  array $withStorage
     * @return $this
     */
    public function withStorageRelations(array $withStorage)
    {
        $relations = $this->getStorageRelations($withStorage);
        foreach ($relations as $name => $relation) {
            $this->withStorageRelations[] = $name;
            $this->setUpRelation($name, $relation);
        }
        return $this;
    }

    public function withRelations()
    {
        # BpOf: array_merge
        $relations = $this->withRootRelations;

        foreach ($this->withStorageRelations as $relation) $relations[] = $relation;

        return $this->newQuery()->with($relations);
    }

    private function setUpRelation($name, $relation)
    {
        static::addDynamicRelation($name, function() use ($relation) {

            [
                'type' => $type,
                'related' => $related,
                'localKey' => $localKey,
                'foreignKey' => $foreignKey,
                'extra' => $extra
            ] = $relation + ['foreignKey' => null, 'extra' => []];

            try {
                $scheme = (new MemoryStorageV2)->memoize(['@scheme', $related['type'], $related['name']], function() use ($related) {
                    return Scheme::where(['name' => $related['name'], 'type' => $related['type']])->first();
                });
            } catch (ModelNotFoundException $e) {
                throw new KlayException("Esquema [schema: {$related['name']}] no existe", '');
            }

            $instance = $scheme->newSchemeType();

            $localKey = $this->localKeysResolve($localKey);

            $foreignKey = $foreignKey ?? $instance->getKeyNameForRelations();

            if ($type == 'hasOne') {
                $builder = (new HasOne($instance->newQuery(), $this, $foreignKey, $localKey))->extra($extra);
            }

            if ($type == 'hasMany') {
                $builder = (new HasMany($instance->newQuery(), $this, $foreignKey, $localKey))->extra($extra);
            }

            return $related['type'] == 'cat' ? $builder->withTrashed() : $builder;
        });
    }
}
