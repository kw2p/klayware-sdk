<?php

namespace Archiving\SDK\Traits;

use Fico7489\Laravel\EloquentJoin\EloquentJoinBuilder;
use Fico7489\Laravel\EloquentJoin\Traits\ExtendRelationsTrait;

/**
 * Trait EloquentJoin.
 *
 * @method static EloquentJoinBuilder joinRelations($relations, $leftJoin = null)
 * @method static EloquentJoinBuilder whereJoin($column, $operator, $value, $boolean = 'and')
 * @method static EloquentJoinBuilder orWhereJoin($column, $operator, $value)
 * @method static EloquentJoinBuilder whereInJoin($column, $values, $boolean = 'and', $not = false)
 * @method static EloquentJoinBuilder whereNotInJoin($column, $values, $boolean = 'and')
 * @method static EloquentJoinBuilder orWhereInJoin($column, $values)
 * @method static EloquentJoinBuilder orWhereNotInJoin($column, $values)
 * @method static EloquentJoinBuilder orderByJoin($column, $direction = 'asc', $aggregateMethod = null)
 */
trait EloquentJoin
{
    use ExtendRelationsTrait;

    /**
     * @param $query
     *
     * @return EloquentJoinBuilder
     */
    public function newEloquentBuilder($query)
    {
        $newEloquentBuilder = new class($query) extends EloquentJoinBuilder {

          function get($columns = ['*']) {
            return $this->buildStorage(function() use ($columns) {
              return parent::get($columns);
            });
          }

          function buildStorage(\Closure $closure) {
            try {
              return $closure();
            } catch (\Illuminate\Database\QueryException $e) {
              if ($e->getCode() == '42S02') {
                $this->model->build($this->model);
                return $closure();
              } else {
                throw new $e($e->getMessage(), $e->getBindings(), $e->getPrevious());
              }
            }
          }
        };

        if (isset($this->useTableAlias)) {
            $newEloquentBuilder->setUseTableAlias($this->useTableAlias);
        }

        if (isset($this->appendRelationsCount)) {
            $newEloquentBuilder->setAppendRelationsCount($this->appendRelationsCount);
        }

        if (isset($this->leftJoin)) {
            $newEloquentBuilder->setLeftJoin($this->leftJoin);
        }

        if (isset($this->aggregateMethod)) {
            $newEloquentBuilder->setAggregateMethod($this->aggregateMethod);
        }

        return $newEloquentBuilder;
    }
}
