<?php

namespace Archiving\SDK\Traits;

use Illuminate\Database\Eloquent\Builder;

trait HasCompositePrimaryKey
{
  /**
   * The composite keys for the model.
   *
   * @var array
   */
  // protected $compositeKeys = [];

  /**
   * Get the composite keys for the model.
   *
   * @return string
   */
  public function getCompositeKeys()
  {
      return $this->compositeKeys;
  }

  /**
   * Set the keys for a save update query.
   *
   * @param  \Illuminate\Database\Eloquent\Builder  $query
   * @return \Illuminate\Database\Eloquent\Builder
   */
  protected function setKeysForSaveQuery($query)
  {
    foreach ($this->getCompositeKeys() as $keyName) {
      $query->where($keyName, '=', $this->getCompositeKeyForSaveQuery($keyName));
    }

    return $query;
  }

  /**
   * Get the composite key value for a save query.
   *
   * @return mixed
   */
  protected function getCompositeKeyForSaveQuery($keyName)
  {
    return $this->original[$keyName] ?? $this->getAttribute($keyName);
  }
}
