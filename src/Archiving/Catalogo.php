<?php

namespace Archiving\SDK;

use Archiving\SDK\Client;
use Archiving\SDK\Entry;
use Archiving\SDK\Exception;
use Illuminate\Support\Collection;

class Catalogo extends Entry
{
    const CATALOG_All_ENDPOINT = 'catalogos:consultar';
    const CATALOG_UPDATE_ENDPOINT = 'catalogos:actualizar';
    const CATALOG_CLEAR_ENDPOINT = 'catalogos:limpiar';
    const CATALOG_REPLACE_ENDPOINT = 'catalogos:remplazar';
    const CATALOG_DELETE_ENDPOINT = 'catalogos:eliminar';
    const CATALOG_VALID_ENDPOINT = 'catalogos:validar';
    const CATALOG_VALID_CLEAR_ENDPOINT = 'catalogos:validar_limpiar';
    const CATALOG_VALID_REPLACE_ENDPOINT = 'catalogos:validar_remplazar';

    public static function consultar($schema, Client $cliente, $params = [], &$paginate = null)
    {
        $data = array_filter([
            'schema' => $schema,
            'with' => $params['with'] ?? null,
            'with-storage' => $params['with-storage'] ?? null,
            'with-trashed' => $params['with-trashed'] ?? null,
            'filter' => $params['filter'] ?? null,
            'order' => $params['order'] ?? null,
            'orderByJoin' => $params['orderByJoin'] ?? null,
            'limit' => $params['limit'] ?? null,
            'page' => $params['page'] ?? null,
        ]);

        $response = $cliente->performJsonRequest('POST', static::CATALOG_All_ENDPOINT, ['body' => $data]);

        $collection = (new static($schema, $cliente))->createCollectionFromResponse($response);

        $paginate = $response['paginate'] ?? $paginate;

        return $collection;
    }

    public static function cargar($schema, string $id, Client $cliente, array $extra_params = [])
    {
        $entry = static::consultar($schema, $cliente, array_only(array_filter($extra_params), ["with", "with-storage"]) + ['filter' => ['where' => [
            ['field' => 'id', 'operator' => '=', 'value' => $id],
        ]]])->first();

        if ($entry === null) {
            (new static($schema, $cliente))->throwNotFoundException("schema: {$schema}, catalogo: {$id}");
        }

        return $entry;
    }

    public function actualizar(array $payload, $id = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'id' => $this->getAttribute('id') ?? $id,
            'payload' => $payload
        ]);

        $response = $this->service->performJsonRequest('POST', static::CATALOG_UPDATE_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function limpiar(array $payload, $id = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'id' => $this->getAttribute('id') ?? $id,
            'payload' => $payload
        ]);

        $response = $this->service->performJsonRequest('POST', static::CATALOG_CLEAR_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function remplazar(array $payload, $id = null)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'id' => $this->getAttribute('id') ?? $id,
            'payload' => $payload
        ]);

        $response = $this->service->performJsonRequest('POST', static::CATALOG_REPLACE_ENDPOINT, ['body' => $data]);

        return $this->createDataFromResponse($response);
    }

    public function eliminar()
    {
        $data = [
            'schema' => $this->schema,
            'id' => $this->id
        ];

        $response = $this->service->performJsonRequest('POST', static::CATALOG_DELETE_ENDPOINT, ['body' => $data]);

        return $this->ifSuccessResponse($response, function($response) {
            return $response;
        });
    }

    public function validarSchema(array $payload)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'id' => $this->id,
            'payload' => $payload
        ]);

        $response = $this->service->performJsonRequest('POST', static::CATALOG_VALID_ENDPOINT, ['body' => $data]);

        return $this->ifSuccessResponse($response, function($response) {
            return $response;
        });
    }

    public function validarLimpiarSchema(array $payload)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'id' => $this->id,
            'payload' => $payload
        ]);

        $response = $this->service->performJsonRequest('POST', static::CATALOG_VALID_CLEAR_ENDPOINT, ['body' => $data]);

        return $this->ifSuccessResponse($response, function($response) {
            return $response;
        });
    }

    public function validarRemplazarSchema(array $payload)
    {
        $data = array_filter([
            'schema' => $this->schema,
            'id' => $this->id,
            'payload' => $payload
        ]);

        $response = $this->service->performJsonRequest('POST', static::CATALOG_VALID_REPLACE_ENDPOINT, ['body' => $data]);

        return $this->ifSuccessResponse($response, function($response) {
            return $response;
        });
    }

    public function getQueryEndpoint()
    {
        return static::CATALOG_All_ENDPOINT;
    }

    public function throwNotFoundException($params)
    {
        throw new Exception("Catalogo [{$params}] no existe", 'archiving.actions.catalogs.load.catalog_nonexist');
    }

}
