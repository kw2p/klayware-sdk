<?php

namespace Archiving\SDK;

use Archiving\SDK\Client;

class Media
{
    const MEDIA_UPLOAD_ENDPOINT = 'media:cargar';
    const MEDIA_GET_ENDPOINT = 'media:obtener';

    public static function cargar($media_file, Client $cliente)
    {
        $data = [[
            'name'     => 'media',
            'filename' => $media_file->getClientOriginalName(),
            'contents' => fopen($media_file->getPathname(), 'r'),
        ]];

        $response = $cliente->performJsonRequest('POST', static::MEDIA_UPLOAD_ENDPOINT, [
            'multipart' => $data
        ]);

        return (new static)->ifSuccessResponse($response, function($response) {
            return $response;
        });
    }

    public static function obtener($media_id, Client $cliente)
    {
        $data = [
            'media' => $media_id,
        ];

        try {
            $response = $cliente->performRequest('POST', static::MEDIA_GET_ENDPOINT, ['body' => $data]);
        } catch (\Exception $e) {
            $response = false;
        }

        return $response;
    }

    /**
     * Execute a callback if response is success
     *
     * @param  array $response
     * @param  \clousure $clousure
     * @return mixed
     */
    public function ifSuccessResponse($response, $clousure)
    {
        if ($response['status'] === 'success') {
            if (!is_callable($clousure)) {
                throw new \InvalidArgumentException('Clousure debe ser un funcion');
            }
            return $clousure($response['data']);
        } else {
            throw new Exception($response['message'], $response['code']);
        }
        return false;
    }
}
