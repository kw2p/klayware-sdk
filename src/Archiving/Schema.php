<?php

namespace Archiving\SDK;

use Archiving\SDK\Client;
use Archiving\SDK\Entry;
use Archiving\SDK\Exception;
use Illuminate\Support\Collection;

class Schema extends Entry
{
    const SCHEMA_All_ENDPOINT = 'schemas:consultar';

    public static function consultar(Client $cliente, $params = [], &$paginate = null)
    {
        $data = array_filter([
            'filter' => $params['filter'] ?? null,
            'limit' => $params['limit'] ?? null,
        ]);

        $response = $cliente->performJsonRequest('POST', static::SCHEMA_All_ENDPOINT, ['body' => $data]);

        $collection = (new static(null, $cliente))->createCollectionFromResponse($response);

        $paginate = $response['paginate'] ?? $paginate;

        return $collection;
    }

    public static function cargar($schema, Client $cliente)
    {
        $entry = static::consultar($cliente, ['filter' => ['where' => [
            ['field' => 'name', 'operator' => '=', 'value' => $schema],
        ]]])->first();

        if ($entry === null) {
            (new static(null, $cliente))->throwNotFoundException("schema: {$schema}");
        }

        return $entry;
    }

    public function throwNotFoundException($params)
    {
        throw new Exception("Schema [{$params}] no existe", 'archiving.actions.schemas.load.schema_nonexist');
    }
}
