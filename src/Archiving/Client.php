<?php

namespace Archiving\SDK;

use Archiving\SDK\Exception;
use Http\Client\Curl\Client as HTTPClient;
use Http\Client\Exception\NetworkException;
use Http\Message\MultipartStream\MultipartStreamBuilder;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Nyholm\Psr7\Factory\HttplugFactory;
use Nyholm\Psr7\Factory\Psr17Factory;

class Client
{
    /**
     * Archiving base url
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Archiving scope endpoint
     *
     * @var string
     */
    protected $baseEndpoint;

    /**
     * Authentication email
     *
     * @var string
     */
    protected $email;

    /**
     * Authentication apikey
     *
     * @var string
     */
    protected $apikey;

    /**
     * Archiving scope
     *
     * @var string
     */
    protected $scope;

    /**
     * HTTP Client
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * HTTP Client request timeout
     *
     * @var int
     */
    protected $timeout = 60;

    public function __construct($options = [])
    {
        if (isset($options['email'])) {
            $this->email = $options['email'];
        }

        if (isset($options['apikey'])) {
            $this->apikey = $options['apikey'];
        }

        if (isset($options['base_url'])) {
            $this->baseUrl = rtrim($options['base_url'], '/');
        }

        $this->baseEndpoint = $this->baseUrl . '/archiving';

        if ($scope = $options['scope'] ?? null) {
            $this->scope = $scope;
            $this->baseEndpoint = $this->baseUrl . sprintf('/archiving:%s', $scope);
        }

        $this->setHTTPClient($this->getDefaultHTTPClient());
    }

    /**
     * Get the base endpoint url
     *
     * @return string
     */
    public function getBaseEndpoint()
    {
        return $this->baseEndpoint;
    }

    /**
     * Get the base url
     *
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * Get the authentication token
     *
     * @return string
     */
    public function getAuthorization()
    {
        return base64_encode("{$this->email}:{$this->apikey}");
    }

    /**
     * Get the Archiving scope
     *
     * @return null|string
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * Set a new Archiving scope
     *
     * @return null|string
     */
    public function setScope($newScope)
    {
        $this->scope = $newScope;
    }

    /**
     * Set the HTTP Client
     *
     * @param HTTPClient $httpClient
     */
    public function setHTTPClient(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Get the HTTP Client
     *
     * @return HTTPClient|null
     */
    public function getHTTPClient()
    {
        return $this->httpClient;
    }

    /**
     * Get the default HTTP Client
     *
     * @return HTTPClient
     */
    public function getDefaultHTTPClient()
    {
        return new HTTPClient(new Psr17Factory, new Psr17Factory);
    }

    /**
     * Perform a Json HTTP Request
     *
     * @param $method
     * @param $path
     * @param array $params
     *
     * @return array
     */
    public function performJsonRequest($method, $path, array $params = [])
    {
        $request = $this->buildRequest($method, $path, $params);
        try {
            $response = $this->httpClient->sendRequest($request);
            $content = json_decode($response->getBody()->getContents(), true);
        } catch (NetworkException $ex) {
            throw new Exception($ex->getMessage(), 'archiving.server_exception');
        }
        return $content;
    }

    /**
     * Perform a HTTP Request
     *
     * @param $method
     * @param $path
     * @param array $params
     *
     * @return array
     */
    public function performRequest($method, $path, array $params = [])
    {
        $request = $this->buildRequest($method, $path, $params);
        try {
            $response = $this->httpClient->sendRequest($request);
        } catch (NetworkException $ex) {
            throw new Exception($ex->getMessage(), 'archiving.server_exception');
        }
        return $response;
    }

    /**
     * Build a request object
     *
     * @param $method
     * @param $path
     * @param $params
     *
     * @return \GuzzleHttp\Message\Request|Request
     */
    public function buildRequest($method, $path, array $params = [])
    {
        $body = $params['body'] ?? null;
        $query = $params['query'] ?? null;
        $multipart = $params['multipart'] ?? null;
        $options = [];
        if (in_array($method, ['POST', 'PUT', 'PATCH']) && $body) {
            $options['body'] = $body;
        }
        if ($query) {
            $options['query'] = $query;
        }
        if ($multipart) {
            $options['multipart'] = $multipart;
        }
        return $this->createRequest($method, $path, $options);
    }

    /**
     * Creates a request for 5.x or 6.x guzzle version
     *
     * @param $method
     * @param $path
     * @param $options
     *
     * @return \GuzzleHttp\Message\Request|\GuzzleHttp\Message\RequestInterface|Request
     */
    public function createRequest($method, $path, $options)
    {
        $headers = [
            'Authorization' => 'Basic ' . $this->getAuthorization(),
        ];

        $uri = (new HttplugFactory)->createUri($this->getBaseEndpoint() . '/' .  $path);

        $body = $options['body'] ?? null;

        if ($body) {
            $headers['Content-Type']  = 'application/json';
            $body = $this->array_map_recursive($body, function($item) {
                if ($item instanceof UploadedFile || $item instanceof File ) {
                    $mime = $item->getMimeType();
                    $content = file_get_contents($item->getPathname());
                    $base64 = base64_encode($content);
                    return "data:{$mime};base64,{$base64}";
                }
                return $item;
            });
            $body = json_encode($body);
        }

        if ($options['query'] ?? false) {
            $query = $options['query'];
            if (is_array($query)) {
                $query = http_build_query($query, null, '&', PHP_QUERY_RFC3986);
            }
            if (!is_string($query)) {
                throw new \InvalidArgumentException('query must be a string or array');
            }
            $uri = $uri->withQuery($query);
        }

        if ($options['multipart'] ?? false) {

            $builder = new MultipartStreamBuilder(new HttplugFactory);

            foreach ($options['multipart'] as ['name' => $name, 'filename' => $filename, 'contents' => $contents]) {
                $builder->addResource($name, $contents, ['filename' => $filename]);
            }

            $headers['Content-Type']  = 'multipart/form-data; boundary="' . $builder->getBoundary() . '"';

            $body = $builder->build();
        }

        return (new HttplugFactory)->createRequest($method, $uri, $headers, $body);
    }

    private function array_map_recursive(array $array, callable $func) {
        array_walk_recursive($array, function(&$v) use ($func) {
            $v = $func($v);
        });
        return $array;
    }
}
